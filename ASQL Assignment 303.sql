USE master
go

DROP DATABASE IF EXISTS EmployeeManagementFSOFT2
go

CREATE DATABASE EmployeeManagementFSOFT2
GO

USE EmployeeManagementFSOFT2
GO

CREATE TABLE Employee
(
	EmpNo INT PRIMARY KEY IDENTITY (1,1),
	EmpName NVARCHAR(50),
	BirthDate DATETIME,
	Email VARCHAR(50) UNIQUE,
	DeptNo INT,
	MgrNo INT NOT NULL,
	StartDate DATETIME,
	Salary MONEY,
	Level TINYINT CHECK (Level >= 1 AND Level <= 7),
	Status TINYINT CHECK(Status = 0 OR Status =1 OR Status =2),
	Note NVARCHAR(100)
)

CREATE TABLE Skill
(
	SkillNo INT PRIMARY KEY IDENTITY (1,1),
	SkillName NVARCHAR (50),
	Note NVARCHAR(100)
)

CREATE TABLE Emp_Skill
(
	SkillNo INT FOREIGN KEY REFERENCES Skill(SkillNo) NOT NULL ,
	EmpNo INT FOREIGN KEY REFERENCES Employee(EmpNo) NOT NULL ,
	SkillLevel TINYINT CHECK(SkillLevel > =1 AND SkillLevel <=3),
	RegDate DATETIME,
	Description NVARCHAR(100)
)

ALTER TABLE Emp_Skill ADD PRIMARY KEY (SkillNo, EmpNo)

CREATE TABLE Department 
(
	DeptNo INT PRIMARY KEY IDENTITY (1,1),
	DeptName NVARCHAR (50),
	Note NVARCHAR(100)
)








-----------------INSERT--------------------
----EMPLOYEE
insert into Employee (EmpName, BirthDate, Email, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Kathryne Loble', '5/15/1993', 'kloble0@yellowpages.com', 6, 2, '1/4/2023', '$1378.00', 5, 2, 'Vivamus tortor.');
insert into Employee (EmpName, BirthDate, Email, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Ilise Choppin', '8/8/2001', 'ichoppin1@prnewswire.com', 7, 2, '1/28/2023', '$1972.71', 6, 0, 'Donec dapibus.');
insert into Employee (EmpName, BirthDate, Email, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Cheston Lardge', '8/29/1992', 'clardge2@t-online.de', 6, 5, '1/11/2023', '$1908.11', 5, 2, 'Nam nulla.');
insert into Employee (EmpName, BirthDate, Email, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Morgana Sydney', '2/9/1992', 'msydney3@comsenz.com', 3, 5, '6/24/2023', '$1339.67', 1, 0, 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.');
insert into Employee (EmpName, BirthDate, Email, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Alasteir Gostling', '10/10/1994', 'agostling4@shinystat.com', 8, 7, '7/2/2023', '$1875.50', 3, 1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.');
insert into Employee (EmpName, BirthDate, Email, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Berget Cosford', '2/6/2000', 'bcosford5@cornell.edu', 4, 6, '12/7/2022', '$1770.59', 4, 1, 'Etiam faucibus cursus urna.');
insert into Employee (EmpName, BirthDate, Email, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Jaquenetta Clogg', '4/9/1995', 'jclogg6@unesco.org', 6, 7, '9/24/2023', '$1592.25', 4, 2, 'Praesent blandit.');
insert into Employee (EmpName, BirthDate, Email, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note) values ('Sherye Iacobacci', '7/17/1994', 'siacobacci7@jugem.jp', 2, 2, '2/6/2023', '$1417.61', 5, 1, 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.');
----SKILL
insert into Skill (SkillName, Note) values ('C++', 'Fusce posuere felis sed lacus.');
insert into Skill (SkillName, Note) values ('C#', 'Sed accumsan felis.');
insert into Skill (SkillName, Note) values ('.NET', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla.');
insert into Skill (SkillName, Note) values ('JAVA', 'Aenean auctor gravida sem.');
insert into Skill (SkillName, Note) values ('SAP', 'Nulla facilisi.');
----Emp_Skill
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (3, 1, 3, '1/1/2023', 'Praesent lectus.');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (1, 2, 3, '1/27/2023', 'Praesent lectus.');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (2, 4, 1, '12/11/2020', 'Praesent lectus.');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (1, 4, 1, '1/2/2021', 'Praesent lectus.');
insert into Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description) values (2, 5, 2, '6/7/2023', 'Praesent lectus.');
----Department
insert into Department (DeptName, Note) values ('Research and Development', 'Praesent lectus.');
insert into Department (DeptName, Note) values ('Services', 'Aliquam erat volutpat.');
insert into Department (DeptName, Note) values ('Accounting', 'Nullam varius.');
insert into Department (DeptName, Note) values ('Human resources', 'Nullam varius.');
insert into Department (DeptName, Note) values ('FA', 'Nullam varius.');
insert into Department (DeptName, Note) values ('Maldives', 'Nullam varius.');
insert into Department (DeptName, Note) values ('Training', 'Quisque ut erat.');
insert into Department (DeptName, Note) values ('Product Management', 'Donec ut mauris eget massa tempor convallis.');

SELECT * FROM Employee
SELECT * FROM Emp_Skill
SELECT * FROM Skill
SELECT * FROM Department

--Q2--Create the tables with the most appropriate/economic field/column constraints & types. Add at least 8 records into each created tables.
SELECT e.EmpName, e.Email, DATEDIFF(MM, e.StartDate, GETDATE()) as 'Month Working'
FROM Employee e
GROUP BY  e.EmpName, e.Email, DATEDIFF(MM, e.StartDate, GETDATE())
HAVING DATEDIFF(MM, e.StartDate, GETDATE()) > 6

--Q3--Specify the names of the employees whore have either �C++� or �.NET� skills.
SELECT e.EmpName, s.SkillName
FROM Employee e
INNER JOIN Emp_Skill es ON es.EmpNo = e.EmpNo
INNER JOIN Skill s ON es.SkillNo = s.SkillNo
WHERE s.SkillName = 'C++' OR  s.SkillName = '.NET'

--Q4--List all employee names, manager names, manager emails of those employees.
SELECT e.EmpName, m.EmpName AS 'MANAGER', m.Email
FROM Employee e
INNER JOIN Employee m ON e.MgrNo = m.EmpNo
--Q5--Specify the departments which have >=2 employees, print out the list of departments� employees right after each department.
SELECT d.DeptName
FROM Department d
INNER JOIN Employee e ON e.DeptNo = d.DeptNo
GROUP BY d.DeptName
HAVING COUNT(e.EmpName) > 2
--Q6--List all name, email and skill number of the employees and sort ascending order by employee�s name.
SELECT e.EmpName, e.Email, COUNT(es.EmpNo) as 'Total Skill'
FROM Employee e
INNER JOIN Emp_Skill es ON es.EmpNo = e.EmpNo
INNER JOIN Skill s ON s.SkillNo = es.SkillNo
GROUP BY e.EmpName, e.Email
HAVING COUNT(es.EmpNo) > 1
--Q7--Use SUB-QUERY technique to list out the different employees (include name, email, birthday) who are working and have multiple skills.
SELECT e.EmpNo, e.EmpName, e.Email, e.BirthDate
FROM Employee e
WHERE e.Status = 0 AND e.EmpNo IN (
				SELECT es.EmpNo
				FROM Emp_Skill es 
				WHERE es.SkillNo IN (
							SELECT s.SkillNo
							FROM Skill s) 
							GROUP BY es.EmpNo 
							HAVING COUNT(es.EmpNo) > 1)
--Q8--Create a view to list all employees are working (include: name of employee and skill name, department name)
CREATE VIEW WorkingEmployee AS
SELECT e.EmpName, s.SkillName, d.DeptName
FROM Employee e
INNER JOIN Department d ON e.DeptNo = d.DeptNo
INNER JOIN Emp_Skill es ON es.EmpNo = e.EmpNo
INNER JOIN Skill s ON es.SkillNo = s.SkillNo
WHERE e.Status = 0

SELECT * FROM WorkingEmployee
